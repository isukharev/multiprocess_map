import os
import pickle
import shutil
from tqdm import tqdm_notebook, tqdm
from concurrent.futures import ProcessPoolExecutor


class PickleWorker(object):
    """
    Pickable decorator for dumping/loading the result
    """
    def __init__(self, func):
        self.func = func

    @staticmethod
    def load_result(path, remove_file=True):
        """
        :return: result
        """
        with open(path, "rb") as inp:
            result = pickle.load(inp)
        if remove_file:
            os.remove(path)
        return result

    @staticmethod
    def dump_result(result, path):
        """
        :return: path
        """
        with open(path, "wb") as output:
            pickle.dump(result, output)
        return path

    def __call__(self, args_and_path):
        """
        :return: path
        """
        args, path = args_and_path
        result = self.func(args)
        PickleWorker.dump_result(result, path)
        return path


def multiprocess_map(func, args, workers=20, use_pickle=False, use_tqdm=True, is_notebook=True,
                     load_results=True, results_dir="./tmp_multiprocess_map"):
    """
    Simple process-based parallel map

    :return: list of results or paths to results
    """
    if workers > len(args):
        workers = len(args)

    if use_pickle:
        if os.path.exists(results_dir):
            shutil.rmtree(results_dir)
        os.mkdir(results_dir)
        args = list(zip(args, map(lambda i: os.path.join(results_dir, "{0:05d}.result".format(i)),
                                  range(len(args)))))
        func = PickleWorker(func)

    if load_results and use_pickle:
        def get_results(results_iterator): return list(map(PickleWorker.load_result, results_iterator))
    else:
        def get_results(results_iterator): return list(results_iterator)

    with ProcessPoolExecutor(max_workers=workers) as executors:
        results_iter = executors.map(func, args)
        if use_tqdm:
            if is_notebook:
                results_iter = tqdm_notebook(results_iter, total=len(args))
            else:
                results_iter = tqdm(results_iter, total=len(args))
        results = get_results(results_iter)
        if load_results and use_pickle:
            shutil.rmtree(results_dir)
        return results
